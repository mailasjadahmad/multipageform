let dataObj = {};
const firstStepBtn = document.querySelector(".first-step-btn");
const firstStepForm = document.querySelector(".form-container-s1");
const secondStepBtn = document.querySelector(".second-step-btn");
const secondStepBackBtn = document.querySelector(".second-step-back-btn");
const secondStepPlanOption = document.querySelector(".plan-container-s2");
const stepOne = document.querySelector(".s-one .step-number");
const stepTwo = document.querySelector(".s-two .step-number");
const stepThree = document.querySelector(".s-three .step-number");
const stepFour = document.querySelector(".s-four .step-number");
let planType = "monthly";

firstStepBtn.addEventListener("click", (e) => {
  e.preventDefault();
  const name = document.querySelector('input[name="user-name"]');
  const email = document.querySelector('input[name="mail-address"]');
  const phone = document.querySelector('input[name="phone-number"]');
  const user = {
    userName: "",
    email: "",
    phone: "",
  };
  user.userName = name.value;
  user.email = email.value;
  user.phone = phone.value;

  var phoneInput = document.querySelector('input[type="tel"]');
  var phonePattern = /^[0-9]{10}$/;
  // console.log(phoneInput)

  if (name.value && email.value && phone.value) {
    secondStepPlanOption.classList.remove("none");
    dataObj["userDetails"] = user;
    stepOne.classList.remove("active-step");
    firstStepForm.classList.add("none");
    stepTwo.classList.add("active-step");
    // console.log(stepOne, stepTwo);
    console.log(secondStepPlanOption);
  }
  for (let key in user) {
    if (key == "phone") {
      // console.log(!phonePattern.test(phoneInput.value))
      if (user[key] == "") {
        const messageField = document.querySelector(".tel-s1 > p");
        messageField.style.display = "flex";
      } else {
        const messageField = document.querySelector(".tel-s1 > p");
        if (!phonePattern.test(phoneInput.value)) {
          messageField.style.display = "flex";
          messageField.textContent = "enter a valid phone number";
        } else {
        }
      }
    } else if (user[key] == "") {
      if (key == "userName") {
        const messageField = document.querySelector(".name-s1 > p");
        messageField.style.display = "flex";
      }
      if (key == "email") {
        const messageField = document.querySelector(".mail-s1 > p");
        messageField.style.display = "flex";
      }
    } else {
      if (key == "userName") {
        const messageField = document.querySelector(".name-s1 > p");
        messageField.style.display = "none";
      }
      if (key == "email") {
        const messageField = document.querySelector(".mail-s1 > p");
        messageField.style.display = "none";
      }
    }
  }
});

secondStepBackBtn.addEventListener("click", () => {
  stepOne.classList.add("active-step");
  firstStepForm.classList.remove("none");
  stepTwo.classList.remove("active-step");
  // console.log(stepOne, stepTwo);
  secondStepPlanOption.classList.add("none");
});

function toggleState() {
  const container = document.querySelector(".toggle-container");
  const indicator = document.querySelector(".toggle-indicator");
  const monthly = document.querySelector(".monthly");
  const yearly = document.querySelector(".yearly");
  const arcadePricing = document.querySelector(
    ".arcade .plan-details .pricing"
  );
  const advancedPricing = document.querySelector(
    ".advanced .plan-details .pricing"
  );
  const proPricing = document.querySelector(".pro .plan-details .pricing");
  const extra2Month = document.querySelectorAll(".yearly-2mnth");

  container.classList.toggle("active-toggle");
  const isActive = container.classList.contains("active-toggle");

  if (isActive) {
    indicator.style.transform = "translateX(28.5px)";
    yearly.classList.toggle("active-toggle");
    arcadePricing.textContent = "$90/yr";
    advancedPricing.textContent = "$120/yr";
    proPricing.textContent = "$150/yr";
    planType = "yearly";
    for (let i = 0; i < extra2Month.length; i++) {
      extra2Month[i].classList.remove("none-el");
    }
    monthly.classList.toggle("active-toggle");
  } else {
    indicator.style.transform = "translateX(0)";
    yearly.classList.toggle("active-toggle");
    monthly.classList.toggle("active-toggle");
    arcadePricing.textContent = "$9/yr";
    advancedPricing.textContent = "$12/yr";
    proPricing.textContent = "$15/yr";
    planType = "monthly";
    for (let i = 0; i < extra2Month.length; i++) {
      extra2Month[i].classList.add("none-el");
    }
  }
  monthly.classList;
}

const plansArr = document.querySelectorAll(".single-plan");
console.log(plansArr);
let planSelected = {
  type: "monthly",
  plan: "arcade",
  amt: "9",
};

for (let planIndex = 0; planIndex < plansArr.length; planIndex++) {
  plansArr[planIndex].addEventListener("click", () => {
    const arcadePricing = document.querySelector(
      ".arcade .plan-details .pricing"
    );
    const advancedPricing = document.querySelector(
      ".advanced .plan-details .pricing"
    );
    const proPricing = document.querySelector(".pro .plan-details .pricing");
    const singlePlan = plansArr[planIndex];

    if (singlePlan.classList.contains("arcade")) {
      const price = arcadePricing.textContent.match(/\d+/g);
      planSelected = {
        type: planType,
        plan: "Arcade",
        amt: price[0],
      };

      singlePlan.classList.add("selected-plan");
      // console.log(singlePlan)
      for (let i = 0; i < plansArr.length; i++) {
        if (plansArr[i] != singlePlan)
          plansArr[i].classList.remove("selected-plan");
      }
      dataObj["planDetails"] = planSelected;
      console.log(dataObj);
    } else if (singlePlan.classList.contains("advanced")) {
      const price = advancedPricing.textContent.match(/\d+/g);
      planSelected = {
        type: planType,
        plan: "Advanced",
        amt: price[0],
      };
      singlePlan.classList.add("selected-plan");
      for (let i = 0; i < plansArr.length; i++) {
        if (plansArr[i] != singlePlan)
          plansArr[i].classList.remove("selected-plan");
      }
      dataObj["planDetails"] = planSelected;
      console.log(dataObj);
    } else {
      const price = proPricing.textContent.match(/\d+/g);
      planSelected = {
        type: planType,
        plan: "Pro",
        amt: price[0],
      };
      singlePlan.classList.add("selected-plan");
      for (let i = 0; i < plansArr.length; i++) {
        if (plansArr[i] != singlePlan)
          plansArr[i].classList.remove("selected-plan");
      }
      dataObj["planDetails"] = planSelected;
      console.log(dataObj);
    }
  });
}

// const secondStepBtn=document.querySelector(".second-step-btn")
const thirdStepBackBtn = document.querySelector(".third-step-back-btn");
const thirdStepAddOn = document.querySelector(".add-ons-outer-container");
thirdStepBackBtn.addEventListener("click", () => {
  stepTwo.classList.add("active-step");
  secondStepPlanOption.classList.remove("none");
  stepThree.classList.remove("active-step");
  // console.log(stepOne, stepTwo);
  thirdStepAddOn.classList.add("none");
});

secondStepBtn.addEventListener("click", () => {
  secondStepPlanOption.classList.add("none");
  thirdStepAddOn.classList.remove("none");
  stepTwo.classList.remove("active-step");
  stepThree.classList.add("active-step");
});

const addonsArr = document.querySelectorAll(".single-checkbox-container");
// console.log(plansArr);
let addonsSelected = {
  // key: { desc: "xyx", price: "$8" },
};

for (let addIndex = 0; addIndex < addonsArr.length; addIndex++) {
  let singleAddon = addonsArr[addIndex];
  singleAddon.addEventListener("click", () => {
    let labelEach = singleAddon.querySelector("label");
    if (labelEach.classList.contains("online-service")) {
      const checkEl = labelEach.querySelector('input[type="checkbox"]');

      if (singleAddon.classList.contains("active-checkbox")) {
        delete addonsSelected.onlineService;
        singleAddon.classList.remove("active-checkbox");
        checkEl.checked = false;
        console.log(addonsSelected, singleAddon);
      } else {
        addonsSelected["onlineService"] = {
          desc: "Access to multiplayer games",
          price: 1,
        };
        singleAddon.classList.add("active-checkbox");
        checkEl.checked = true;
      }
      dataObj["addOns"] = addonsSelected;
    } else if (labelEach.classList.contains("larger-storage")) {
      const checkEl = labelEach.querySelector('input[type="checkbox"]');

      if (singleAddon.classList.contains("active-checkbox")) {
        delete addonsSelected.onlineService;
        singleAddon.classList.remove("active-checkbox");
        checkEl.checked = false;
        console.log(addonsSelected, singleAddon);
      } else {
        addonsSelected["largerStorage"] = {
          desc: "Extra 1TB of cloud save",
          price: 2,
        };
        singleAddon.classList.add("active-checkbox");
        console.log(addonsSelected);
        checkEl.checked = true;
      }
      dataObj["addOns"] = addonsSelected;
    } else {
      const checkEl = labelEach.querySelector('input[type="checkbox"]');

      if (singleAddon.classList.contains("active-checkbox")) {
        delete addonsSelected.onlineService;
        singleAddon.classList.remove("active-checkbox");
        checkEl.checked = false;
        console.log(addonsSelected, singleAddon);
      } else {
        addonsSelected["customizableProfile"] = {
          desc: "Custom theme on your profile",
          price: 2,
        };
        singleAddon.classList.add("active-checkbox");
        console.log(addonsSelected);
        checkEl.checked = true;
      }
      dataObj["addOns"] = addonsSelected;
      console.log(dataObj);
    }
  });
}

const thirdStepBtn = document.querySelector(".third-step-btn");
const billingSummary = document.querySelector(".billing-summary-container");
thirdStepBtn.addEventListener("click", () => {
  thirdStepAddOn.classList.add("none");
  billingSummary.classList.remove("none");
  console.log(billingSummary);
  stepThree.classList.remove("active-step");
  stepFour.classList.add("active-step");
  FillBillingData(dataObj);
});

function FillBillingData(dataObj) {
  let totalPrice = 0;
  const planName = document.querySelector(".header-details .bill-title");
  // const planType = document.querySelector(".bill-title .type");
  const planPrice = document.querySelector(".bill-header-price");
  const billAddOnContainer = document.querySelector(".bill-add-ons");
  let shortForm = dataObj["planDetails"]["type"] == "monthly" ? "mo" : "yr";
  console.log(dataObj["planDetails"]["type"], planType);
//   <p class="bill-title">
//   Arcade(<span>Monthly</span>)
// </p>;
  planName.innerHTML = `${dataObj["planDetails"]["plan"]}<span class="type"></span>`;
  console.log(planName)
  // planType.textContent = dataObj["planDetails"]["type"];
  planPrice.textContent = `$${dataObj["planDetails"]["amt"]}/${shortForm}`;
  totalPrice += Number(dataObj["planDetails"]["amt"]);

  for (let key in dataObj["addOns"]) {
    let sAdOn = dataObj["addOns"][key];
    let billSingleAddOn = document.createElement("div");
    billSingleAddOn.classList.add("bill-single-add-on");
    let singleAddOntitle = document.createElement("p");
    let singleAddOnprice = document.createElement("p");

    singleAddOntitle.textContent = key;
    let singleAddonPriceAfterCal =
      shortForm == "yr" ? sAdOn["price"] * 10 : sAdOn["price"];

   

    singleAddOnprice.textContent = `+${singleAddonPriceAfterCal}/${shortForm}`;
    totalPrice += Number(singleAddonPriceAfterCal);
    billSingleAddOn.appendChild(singleAddOntitle);
    billSingleAddOn.appendChild(singleAddOnprice);
    billAddOnContainer.append(billSingleAddOn);
  }
  const billFinalType = document.querySelector(".bill-final-amt p span");
  const billFinalPrice = document.querySelector(".bill-final-amt .final-price");

  billFinalType.textContent = dataObj["planDetails"]["type"];
  billFinalPrice.textContent = `$${totalPrice}/${shortForm}`;
  // addonsSelected["onlineService"]={
  //   "desc":"Access to multiplayer games",
  //   "price":1
  // }
}
const fourthStepBackBtn=document.querySelector(".fourth-step-back-btn")
const fourthStepBtn=document.querySelector(".fourth-step-btn")

fourthStepBackBtn.addEventListener("click", () => {
  // console.log(billingSummary)
  stepThree.classList.add("active-step");
  thirdStepAddOn.classList.remove("none");
  stepFour.classList.remove("active-step");
  // console.log(stepOne, stepTwo);
  billingSummary.classList.add("none");
});
const thankYouPage=document.querySelector(".thankyou-page-outer")
fourthStepBtn.addEventListener("click",()=>{
  billingSummary.classList.add("none");
  thankYouPage.classList.remove("none")
})

